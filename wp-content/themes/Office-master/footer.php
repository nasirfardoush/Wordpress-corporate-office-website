 <?php global $redux_office; ?>
 <!-- Footer -->
    <footer style="background: <?php echo $redux_office['footer_top_bg'];?> ;"> 
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h3><i class="fa <?php echo $redux_office['col_1_icon']; ?>"></i><?php echo $redux_office['col_1_title']; ?></h3>
                    <?php echo $redux_office['col_1_text']; ?>
                </div>
                <div class="col-md-4">
                    <h3><i class="fa <?php echo $redux_office['col_2_icon']; ?>"></i> <?php echo $redux_office['col_2_title']; ?></h3>
                    <?php 
                    if(is_array($redux_office['col_2_links'])){
                    foreach ($redux_office['col_2_links'] as $singlelink) {
                       echo '<p> <a href="'.$singlelink['url'].'">'.$singlelink['title'].'</a></p>';
                    } }?>
                </div>
              <div class="col-md-4">
                <h3><i class="fa <?php echo $redux_office['col_3_icon']; ?>"></i> <?php echo $redux_office['col_3_title']; ?></h3>
                <div id="social-icons">
                    <?php
                    if(is_array($redux_office['col_3_links'])){
                    foreach ($redux_office['col_3_links'] as $singleicon) {
                       echo '<a href="'.$singleicon['url'].'" class="btn-group google-plus"><i class="fa '.$singleicon['title'].'"></i></a>';
                    } }?>
                    </a>
                </div>
              </div>    
        </div>
      </div>
    </footer>

    
    <div class="copyright text center">
        <?php  echo $redux_office['copyright_text'];?>
    </div>
    <!-- Alscripts include here form functons.php -->
    <?php wp_footer(); ?>
  </body> 
</html>
