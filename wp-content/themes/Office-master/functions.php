<?php
//function for dynamic title and all theme support
function office_master_theme_support(){
	add_theme_support( 'title-tag' );
	add_theme_support('post-thumbnails');
	add_image_size( 'img_size', 1500,500 , true );
	add_image_size( 'sidber_slide_img', 300,192 , true );
	add_image_size( 'team_img_size', 100,80 , true );
	add_image_size( 'post_img_size', 850,460 , true );

	register_nav_menus(array(
		'primary_menu'=>'Primary Menu'
	));		
}
add_action('after_setup_theme','office_master_theme_support');


//function for all scripts
function office_master_css_js(){ 
	wp_enqueue_style( 'google-font-1', '//fonts.googleapis.com/css?family=Open+Sans:400,300','null','v1.0','all' );
	wp_enqueue_style('google-font-2','//fonts.googleapis.com/css?family=PT+Sans','null','v1.0','all');
	wp_enqueue_style('google-font-3','fonts.googleapis.com/css?family=Raleway','null','v1.0','all');
	wp_enqueue_style('bootstrap',get_template_directory_uri().'/assets/bootstrap/css/bootstrap.css','null','v1.0','all');
	wp_enqueue_style('font-awesome',get_template_directory_uri().'/assets/css/font-awesome.min.css','null','v1.0','all');
	wp_enqueue_style('theme-style',get_template_directory_uri().'/assets/css/style.css','null','v1.0','all');
	wp_enqueue_style('animate',get_template_directory_uri().'/assets/css/animate.min.css','null','v1.0','all');
	wp_enqueue_style('mandetory-style', get_stylesheet_uri());
	
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/assets/bootstrap/js/bootstrap.min.js', 'jquery', 'nul', true );
	wp_enqueue_script( 'wow-js', get_template_directory_uri().'/js/wow.min.js', 'jquery', 'nul', true );
}
add_action('wp_enqueue_scripts','office_master_css_js');


//function for ad spacific script
function fotter_extra_scripts(){ ?>
    <script>
      new WOW().init();
    </script>
<?php }
add_action('wp_footer','fotter_extra_scripts',50);//here 50 used for scripts ordering.


//Defult menu
function fallback_menu(){ ?>
	<ul class="nav navbar-nav pull-right">
	    <li class="active">
	        <a href="#">Home</a>
	    </li>
	    <li>
	        <a href="#">About</a>
	    </li>
	    <li>
	        <a href="#">Blog</a>
	    </li>
	    <li>
	        <a href="#">Team</a>
	    </li>
	    <li>
	        <a href="#"><span>Contact</span></a>
	    </li>
	</ul>
<?php }




//include Custom post
include_once('inc/custom-post.php');
//include cmb2 metabox
include_once('inc/cmb2_custom.php');
//include redux framework
require_once('inc/redux-framework-master/redux-framework.php');
require_once('inc/office-theme-option-redux.php');