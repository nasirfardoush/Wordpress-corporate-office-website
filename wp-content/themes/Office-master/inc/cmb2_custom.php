<?php
/**
 * Get the bootstrap!
 */
if ( file_exists(  __DIR__ . '/cmb2/init.php' ) ) {
  require_once  __DIR__ . '/cmb2/init.php';
} elseif ( file_exists(  __DIR__ . '/CMB2/init.php' ) ) {
  require_once  __DIR__ . '/CMB2/init.php';
}

add_action( 'cmb2_admin_init','office_master_cmb2');

function office_master_cmb2(){

	$prefix= '_pref_';
	//Service metabox
	$service_item = new_cmb2_box(array(
		'id'		  => 'service_metabox',
		'title'		  => __('Service metabox','office_master'),
		'object_types'=> array('services',),//post type
		'context'	  => 'normal',
		'priority'    => 'high',
		'show_names'  => true,
	));

	$service_item->add_field( array(
		'name' => __('Service icon','office_master'),
		'desc' => __('Add your font-awsome icon\'s name.','office_master'),
		'id'   => $prefix.'service_icon',
		'type' =>'text',

	));

	$service_item->add_field(array(
		'name'=> __('Service description','office_master'),
		'desc'=>__('Write here your service description','office_master'),
		'id'=> $prefix.'service_desc',
		'type'=>'textarea_small',

	));
	$service_item->add_field(array(
		'name' => __('Service button name','office_master'),
		'desc' => __('Write here your service button name','office_master'),
		'id'   => $prefix.'service_btn_name',
		'type' => 'text',
	));

	$service_item->add_field(array(
		'name' => __('Service button url','office_master'),
		'desc' => __('Write here your service button url','office_master'),
		'id'   => $prefix.'service_btn_url',
		'type' => 'text_url',
	));
	$service_item->add_field(array(
		'name' => __('Service animate name','office_master'),
		'desc' => __('Write here your service animate name','office_master'),
		'id'   => $prefix.'service_animate_cl',
		'type' => 'text',
	));



	//Slider  metabox 
	$slider_item = new_cmb2_box(array(
		'id'		  => 'slider',
		'title'		  => __('Slider metabox','office_master'),
		'object_types'=> array('slider',),//post type
		'context'	  => 'normal',
		'priority'    => 'high',
		'show_names'  => true,
	));
	$slider_item->add_field(array(
		'name' => __('Slider caption','office_master'),
		'desc' => __('Set your slider caption','office_master'),
		'id'   => $prefix.'slider_caption',
		'type' => 'text',
	));

	//For team member
	$team_member = new_cmb2_box(array(
		'id'		  => 'team_member',
		'title'		  => __('Team metabox','office_master'),
		'object_types'=> array('team',),//post type
		'context'	  => 'normal',
		'priority'    => 'high',
		'show_names'  => true,
	));
	$team_member->add_field(array(
		'name' => __('Member designition','office_master'),
		'desc' => __('Set Member designition ','office_master'),
		'id'   => $prefix.'member_desig',
		'type' => 'text',
	));
	$team_member->add_field(array(
		'name' => __('Block color class','office_master'),
		'desc' => __('Set Block color class name ','office_master'),
		'id'   => $prefix.'block_color',
		'type' => 'text',
	));
	$team_member->add_field(array(
		'name' => __('Block animate class','office_master'),
		'desc' => __('Block animate class name ','office_master'),
		'id'   => $prefix.'animate_cl',
		'type' => 'text',
	));		
	//For post title icon blog page
	$post_icons = new_cmb2_box(array(
		'id'		  => 'post_icon',
		'title'		  => __('Post metabox','office_master'),
		'object_types'=> array('post',),//post type
		'context'	  => 'normal',
		'priority'    => 'high',
		'show_names'  => true,
	));
	$post_icons->add_field(array(
		'name' => __('Post icon class','office_master'),
		'desc' => __('Set your title icon class name ','office_master'),
		'id'   => $prefix.'post_icon',
		'type' => 'text',
	));
	//For about page sidber groupe  field
	$about_page_sidber = new_cmb2_box(array(
		'id'		  => 'about_page_sidber',
		'title'		  => __('About page group metabox','office_master'),
		'object_types'=> array('page',),//post type
		'context'	  => 'normal',
		'priority'    => 'high',
		'show_names'  => true,
		'show_on'     => array( 
			'key' => 'id', 
			'value' => 9 ), 
	));
	$Group_data = $about_page_sidber->add_field(array(
		'name' => __('Group by add your value','office_master'),
		'desc' => __('You can set every value by add group','office_master'),
		'id'   => $prefix.'group_field',
		'type' => 'group',
	));	
		$about_page_sidber->add_group_field($Group_data,array(
		'name' => __('Sidber heading','office_master'),
		'desc' => __('You can set Sidber heading','office_master'),
		'id'   => $prefix.'group_s_heding',
		'type' => 'text',
	));	
	
			$about_page_sidber->add_group_field($Group_data,array(
		'name' => __('Sidber description','office_master'),
		'desc' => __('Write your sidber description','office_master'),
		'id'   => $prefix.'group_s_desc',
		'type' => 'text_small',
	));	
		$about_page_sidber->add_group_field($Group_data,array(
		'name' => __('Content hase link','office_master'),
		'desc' => __('Set your content hase link','office_master'),
		'id'   => $prefix.'group_s_link',
		'type' => 'text',
		'repeatable'=> true,
	));
		$about_page_sidber->add_group_field($Group_data,array(
		'name' => __('Content hase link title','office_master'),
		'desc' => __('Set your content hase link title','office_master'),
		'id'   => $prefix.'group_s_link_title',
		'type' => 'text',
		'repeatable'=> true,
	));				
}

